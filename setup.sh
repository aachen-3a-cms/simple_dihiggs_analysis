#!/usr/bin/env bash

action() {
    local origin="$( /bin/pwd )"

    #
    # global variables
    #

    export SDHA_BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && /bin/pwd )"

    # check if we're on lxplus
    [[ "$( hostname )" = lxplus*.cern.ch ]] && SDHA_ON_LXPLUS="1" || SDHA_ON_LXPLUS="0"
    export SDHA_ON_LXPLUS

    # set the local data directory
    if [ -z "$SDHA_DATA" ]; then
        export SDHA_DATA="/net/scratch_cms3a/fackeldey/simple_dihiggs_analysis"
    fi

    # complain when no grid user is set
    if [ -z "$SDHA_GRID_USER" ]; then
        2>&1 echo "please set the SDHA_GRID_USER to your grid user name and try again"
        return "1"
    fi

    # set defaults of other variables
    [ -z "$SDHA_SOFTWARE" ] && export SDHA_SOFTWARE="$SDHA_DATA/software/$( whoami )"
    [ -z "$SDHA_STORE" ] && export SDHA_STORE="$SDHA_DATA/store"
    [ -z "$SDHA_LOCAL_CACHE" ] && export SDHA_LOCAL_CACHE="$SDHA_DATA/cache"
    [ -z "$SDHA_GRID_VO" ] && export SDHA_GRID_VO="cms:/cms/dcms"


    #
    # law setup
    #

    # law and luigi setup
    export LAW_HOME="$SDHA_BASE/.law"
    export LAW_CONFIG_FILE="$SDHA_BASE/law.cfg"
    export LAW_TARGET_TMP_DIR="$SDHA_DATA/tmp"

    # variables that are used in law.cfg and depend on whether we run on the grid or not
    if [ "$SDHA_ON_GRID" == "1" ]; then
        export SDHA_LUIGI_WORKER_KEEP_ALIVE="False"
        export SDHA_LUIGI_WORKER_FORCE_MULTIPROCESSING="True"
    else
        export SDHA_LUIGI_WORKER_KEEP_ALIVE="True"
        export SDHA_LUIGI_WORKER_FORCE_MULTIPROCESSING="False"
    fi

    # print a warning when no luigi scheduler host is set
    if [ -z "$SDHA_SCHEDULER_HOST" ]; then
        2>&1 echo "NOTE: SDHA_SCHEDULER_HOST is not set, use '--local-scheduler' in your tasks!"
    fi


    #
    # CMSSW setup
    #

    local scram_cores="$SCRAM_CORES"
    [ -z "$scram_cores" ] && scram_cores="1"

    export SCRAM_ARCH="slc7_amd64_gcc630"
    export CMSSW_VERSION="CMSSW_9_4_12"
    export CMSSW_BASE="$SDHA_SOFTWARE/cmssw/$CMSSW_VERSION"

    source "/cvmfs/cms.cern.ch/cmsset_default.sh"

    # compile if the CMSSW is not there yet
    if [ ! -d "$CMSSW_BASE" ]; then
        mkdir -p "$( dirname "$CMSSW_BASE" )"
        cd "$( dirname "$CMSSW_BASE" )"
        scramv1 project CMSSW "$CMSSW_VERSION"
        cd "$CMSSW_VERSION/src"
        eval `scramv1 runtime -sh`
        scram b -j "$scram_cores"
    fi

    cd "$CMSSW_BASE/src"
    eval `scramv1 runtime -sh`
    cd "$origin"


    #
    # software setup
    #

    # software helper functions
    sdha_install_pip() {
        pip install --no-cache-dir --prefix "$SDHA_SOFTWARE" "$@"
    }
    [ ! -z "$BASH_VERSION" ] && export -f sdha_install_pip

    _addpy() {
        [ ! -z "$1" ] && export PYTHONPATH="$1:$PYTHONPATH"
    }

    _addbin() {
        [ ! -z "$1" ] && export PATH="$1:$PATH"
    }

    # apd software paths
    _addbin "$SDHA_SOFTWARE/bin"
    _addpy "$SDHA_SOFTWARE/lib/python2.7/site-packages"

    # software that is used in this project
    sdha_install_software() {
        local mode="$1"

        if [ -d "$SDHA_SOFTWARE" ]; then
            if [ "$mode" = "force" ]; then
                echo "remove software in $SDHA_SOFTWARE"
                rm -rf "$SDHA_SOFTWARE"
            else
                if [ "$mode" != "silent" ]; then
                    echo "software already installed in $SDHA_SOFTWARE"
                fi
                return "0"
            fi
        fi

        echo "installing software stack in $SDHA_SOFTWARE"
        mkdir -p "$SDHA_SOFTWARE"

        sdha_install_pip docutils
        sdha_install_pip backports.lzma
        sdha_install_pip uproot
        sdha_install_pip slackclient
        sdha_install_pip python-telegram-bot
        sdha_install_pip git+https://github.com/riga/scinum.git
        sdha_install_pip git+https://github.com/riga/order.git
        sdha_install_pip luigi==2.7.6
        LAW_INSTALL_CUSTOM_SCRIPT="1" sdha_install_pip --no-dependencies git+https://github.com/riga/law.git
    }
    [ ! -z "$BASH_VERSION" ] && export -f sdha_install_software

    # install software silent (do nothing when it already exists)
    sdha_install_software silent

    # tune globus thread model for gfal2
    export GLOBUS_THREAD_MODEL="none"

    # little fix for gfal2 python-bindings to link correct libraries again after CMSSW setup
    # but before, save the old LD_LIBRARY_PATH for dark times
    export CMSSW_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
    export LD_LIBRARY_PATH="/cvmfs/grid.cern.ch/centos7-ui-4.0.3-1_umd4v3/usr/lib64:/cvmfs/grid.cern.ch/centos7-ui-4.0.3-1_umd4v3/usr/lib:$LD_LIBRARY_PATH"

    # add _this_ repo
    _addpy "$SDHA_BASE"

    # source law's bash completion scipt
    source "$( law completion )"


    #
    # helper functions
    #

    # vinit() {
    #     local voms="$SDHA_GRID_VO"
    #     [ ! -z "$1" ] && voms="$1"
    #     local valid="196:00"
    #     [ ! -z "$2" ] && valid="$2"
    #     voms-proxy-init -voms "$voms" --valid "$valid"
    # }
    # [ ! -z "$BASH_VERSION" ] && export -f vinit
}
action "$@"
