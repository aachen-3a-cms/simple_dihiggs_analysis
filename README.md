# Simple di-Higgs Analysis (bbWW DL)

### Infos on git

##### `[user]` config

Make sure to have your name and email as registered at CERN in the `.git/config` of the cloned repository. Example:

```
[user]
    name = My Name
    email = my.mail@cern.ch
```


##### LFS

This repository has git-lfs enabled. Therefore, after cloning the repository, run

```shell
git lfs install
```

once.


### Setup

All analysis-specific environment variables are prefixed with `SDHA`.

Before you source the `setup.sh` script, export a few variables that do not belong in any repository:

1. Export your grid user name into `$SDHA_GRID_USER`.
2. Export `$SDHA_SCHEDULER_HOST` to configure the luigi scheduler to use. Optional.
3. Export `$SDHA_SLACK_TOKEN` and `$SDHA_SLACK_CHANNEL` to receive slack notifications. Optional.

# TEMPORARY
If you plan to use HTCondorWorkflow, do:
1. `export X509_USER_PROXY="$HOME/.vomsproxy"`
2. `vinit`
3. add the following law options to the law run command: `--workflow htcondor --no-poll --retries 0 --htcondor-logs`
4. check status of jobs either with: `condor_q` or with `python condor_status.py`

Now, run the setup.

```shell
source setup.sh
law db
```


### Running Tasks

Run the full selection with:

* `law run sdha.SelectionWrapper --local-scheduler --version dev1 --sdha.Selection-workflow htcondor --sdha.Selection-no-poll`
