#!/usr/bin/env bash

load_replica() {
    local remote_base="$1"
    local bundle_re="$2"
    local arc_path="$3"

    local arc="$( gfal-ls "$remote_base" | grep -Po "$bundle_re" | shuf -n 1 )"
    if [ -z "$arc" ]; then
        >&2 echo "could not determine archive to load from $remote_base"
        return "1"
    fi

    gfal-copy "$remote_base/$arc" "$arc_path"
    if [ "$?" != "0" ]; then
        >&2 echo "could not load archive $arc from $remote_base"
        return "1"
    fi
}

action() {
    #
    # set env variables
    #

    export PATH_ORIG="$PATH"
    export PYTHONPATH_ORIG="$PYTHONPATH"
    export LD_LIBRARY_PATH_ORIG="$LD_LIBRARY_PATH"

    export SDHA_DATA="$HOME/sdha_data"
    export SDHA_SOFTWARE="$SDHA_DATA/software"
    export SDHA_STORE="$SDHA_DATA/store"
    export SDHA_LOCAL_CACHE="$SDHA_DATA/cache"
    export SDHA_GRID_USER="{{sdha_grid_user}}"

    export SCRAM_ARCH="{{scram_arch}}"
    export CMSSW_VERSION="{{cmssw_version}}"
    export CMSSW_BASE="$SDHA_DATA/cmssw/$CMSSW_VERSION"

    export SDHA_ON_GRID="1"

    mkdir -p "$SDHA_DATA"


    #
    # setup CMSSW
    #

    source "/cvmfs/cms.cern.ch/cmsset_default.sh"
    mkdir -p "$( dirname "$CMSSW_BASE" )"
    cd "$( dirname "$CMSSW_BASE" )"
    scramv1 project CMSSW "$CMSSW_VERSION"
    cd "$CMSSW_VERSION"
    load_replica "{{cmssw_base_url}}" "$CMSSW_VERSION\.\d+\.tgz" "cmssw.tgz"
    tar -xzf "cmssw.tgz"
    rm "cmssw.tgz"
    cd src
    eval `scramv1 runtime -sh`
    scram build
    cd "$HOME"


    #
    # load the software bundle
    #

    mkdir -p "$SDHA_SOFTWARE"
    cd "$SDHA_SOFTWARE"
    load_replica "{{software_base_url}}" "software\.\d+\.tgz" "software.tgz"
    tar -xzf "software.tgz"
    rm "software.tgz"
    cd "$HOME"


    #
    # load the repo bundle
    #

    load_replica "{{repo_base}}" "simple_dihiggs_analysis\.{{repo_checksum}}\.\d+\.tgz" "repo.tgz"
    tar -xzf "repo.tgz"
    rm "repo.tgz"

    # source the repo setup
    source "simple_dihiggs_analysis/setup.sh"
}
action "$@"
