# coding: utf-8

import six
import ROOT


class Weighter(object):

    def __init__(self, sf_file, hist_name):

        self._sf_file = sf_file
        self._hist_name = hist_name
        self.tfile = ROOT.TFile(self.sf_file)
        self.hist = self.tfile.Get(self.hist_name)

    @property
    def sf_file(self):
        return self._sf_file

    @sf_file.setter
    def sf_file(self, filename):
        if not isinstance(filename, six.string_types):
            raise TypeError("SF filename has to be a string")
        else:
            self._sf_file = filename

    @property
    def hist_name(self):
        return self._hist_name

    @hist_name.setter
    def hist_name(self, filename):
        if not isinstance(filename, six.string_types):
            raise TypeError("Histogram name has to be a string")
        else:
            self.hist_name = filename

    def getWeight1D(self, x):
        bin = self.hist.FindFixBin(x)
        weight = self.hist.GetBinContent(bin)
        return weight

    def getUnc1D(self, x):
        bin = self.hist.FindFixBin(x)
        unc = self.hist.GetBinError(bin)
        return unc

    def getWeight2D(self, x, y):
        bin = self.hist.FindFixBin(x, y)
        weight = self.hist.GetBinContent(bin)
        return weight

    def getUnc2D(self, x, y):
        bin = self.hist.FindFixBin(x, y)
        unc = self.hist.GetBinError(bin)
        return unc

    # in case POG forgot to fill overflow bins...
    def fillOverflow2D(self):
        nbins_x = self.hist.GetNbinsX()
        nbins_y = self.hist.GetNbinsY()

        for i in range(nbins_x + 2):
            if not self.hist.GetBinContent(i, 0):
                self.hist.SetBinContent(i, 0, self.hist.GetBinContent(i, 1))
            if not self.hist.GetBinContent(i, nbins_y + 1):
                self.hist.SetBinContent(i, nbins_y + 1, self.hist.GetBinContent(i, nbins_y))

        for i in range(nbins_y + 2):
            if not self.hist.GetBinContent(0, i):
                self.hist.SetBinContent(0, i, self.hist.GetBinContent(1, i))
            if not self.hist.GetBinContent(nbins_y + 1, i):
                self.hist.SetBinContent(nbins_y + 1, 1, self.hist.GetBinContent(nbins_y, i))

    def __repr__(self):
        repr = "{}".format(self.__class__.__name__)
        repr += "(SF file @ {})".format(self.sf_file)
        return repr


def electron_scale_factors(event, reco_weighter_lowpt, reco_weighter_highpt, id_weighter):

    scale_factors = []
    # loop over electrons
    for i in range(event.nElectron):
        if event.Electron_pt[i] > 20:
            reco_scale_factor = reco_weighter_highpt.getWeight2D(
                event.Electron_eta[i],
                event.Electron_pt[i],
            )
        else:
            reco_scale_factor = reco_weighter_lowpt.getWeight2D(
                event.Electron_eta[i],
                event.Electron_pt[i],
            )
        id_scale_factor = id_weighter.getWeight2D(
            event.Electron_eta[i],
            event.Electron_pt[i],
        )
        # full electron SF
        scale_factor = id_scale_factor * reco_scale_factor
        scale_factors.append(scale_factor)

    return scale_factors


def muon_scale_factors(event, iso_weighter, id_weighter):

    scale_factors = []
    # loop over muons
    for i in range(event.nMuon):
        iso_scale_factor = iso_weighter.getWeight2D(
            event.Muon_pt[i],
            abs(event.Muon_eta[i]),
        )
        id_scale_factor = id_weighter.getWeight2D(
            event.Muon_pt[i],
            abs(event.Muon_eta[i]),
        )
        # full muon SF
        scale_factor = id_scale_factor * iso_scale_factor
        scale_factors.append(scale_factor)

    return scale_factors


def roccor_scale_factors(event, is_data, roccor):

    scale_factors = []
    gRandom = ROOT.TRandom()
    for i in range(event.nMuon):
        scale_factor = 1.
        if is_data:
            scale_factor = roccor.kScaleDT(
                event.Muon_charge[i],
                event.Muon_pt[i],
                event.Muon_eta[i],
                event.Muon_phi[i],
                0,
                0,
            )
            scale_factors.append(scale_factor)
        else:
            if event.Muon_genPartIdx[i] < 0:
                continue
            if abs(event.GenPart_pdgId[event.Muon_genPartIdx[i]]) == 13:
                scale_factor = roccor.kSpreadMC(
                    event.Muon_charge[i],
                    event.Muon_pt[i],
                    event.Muon_eta[i],
                    event.Muon_phi[i],
                    event.GenPart_pt[event.Muon_genPartIdx[i]],
                    0,
                    0,
                )
                scale_factors.append(scale_factor)
            else:
                scale_factor = roccor.kSmearMC(
                    event.Muon_charge[i],
                    event.Muon_pt[i],
                    event.Muon_eta[i],
                    event.Muon_phi[i],
                    event.Muon_nTrackerLayers[i],
                    gRandom.Rndm(),
                    0,
                    0,
                )
                scale_factors.append(scale_factor)

    return scale_factors
