# coding: utf-8


from collections import OrderedDict

import numpy as np
import law
from numba import jit


def build_selection_struct(config_inst):
    struct = OrderedDict()

    # helper to register a new struct entry
    def register(name, dtype, requires=None, selector=None, **kwargs):
        requires = [] if not requires else law.util.make_list(requires)
        selector = law.util.make_tuple(selector or name)
        struct[name] = dict(dtype=dtype, requires=requires, selector=selector, **kwargs)
        return struct[name]

    # get a list of all triggers
    triggers = sum((config_inst.get_aux("triggers")[ch] for ch in config_inst.channels), [])

    for i in range(2):
        register("Electron{}_pt".format(i + 1), np.float, "Electron_pt", ("Electron_pt", i))
        register("Electron{}_eta".format(i + 1), np.float, "Electron_eta", ("Electron_eta", i))
        register("Electron{}_phi".format(i + 1), np.float, "Electron_phi", ("Electron_phi", i))
        register("Electron{}_mass".format(i + 1), np.float, "Electron_mass", ("Electron_mass", i))
        register("Electron{}_charge".format(i + 1), np.int, "Electron_charge", ("Electron_charge", i))
        register("Muon{}_pt".format(i + 1), np.float, "Muon_pt", ("Muon_pt", i))
        register("Muon{}_eta".format(i + 1), np.float, "Muon_eta", ("Muon_eta", i))
        register("Muon{}_phi".format(i + 1), np.float, "Muon_phi", ("Muon_phi", i))
        register("Muon{}_mass".format(i + 1), np.float, "Muon_mass", ("Muon_mass", i))
        register("Muon{}_charge".format(i + 1), np.int, "Muon_charge", ("Muon_charge", i))
    for i in range(4):
        register("Jet{}_pt".format(i + 1), np.float, "Jet_pt", ("Jet_pt", i))
        register("Jet{}_eta".format(i + 1), np.float, "Jet_eta", ("Jet_eta", i))
        register("Jet{}_mass".format(i + 1), np.float, "Jet_mass", ("Jet_mass", i))
        register("Jet{}_btagDeepB".format(i + 1), np.float, "Jet_btagDeepB", ("Jet_btagDeepB", i))
    register("nGoodElectron", np.int, ["Electron_pfRelIso03_all", "Electron_cutBased"])
    register("nGoodMuon", np.int, ["Muon_pfRelIso04_all", "Muon_mediumId"])
    register("nGoodJet", np.int, "Jet_jetId")
    register("nGoodBTag", np.int, "Jet_btagDeepB")
    register("mll", np.float)
    register("channelId", np.int, triggers)
    register("Generator_weight", np.float, "Generator_weight")
    # https://twiki.cern.ch/twiki/bin/view/CMS/MissingETOptionalFiltersRun2#2017_data
    register("METFilters", np.bool, [
        "Flag_goodVertices", "Flag_globalSuperTightHalo2016Filter", "Flag_HBHENoiseFilter",
        "Flag_HBHENoiseIsoFilter", "Flag_EcalDeadCellTriggerPrimitiveFilter",
        "Flag_BadPFMuonFilter", "Flag_eeBadScFilter",
    ])

    return struct


def build_event_selection(config_inst, struct, read_keys):
    """
    Return a function that decides if an event is selected or not. In this _outer_ function, common
    objects can be obtained to increase performance by removing as much code as possible from event
    loops.
    Detailed definitions of nanoAOD branches can be found here:
    https://cms-nanoaod-integration.web.cern.ch/integration/master/mc94X_doc.html
    """
    # triggers in the config might be patterns, gather real paths from event keys per channel
    # via globbing through law.util.multi_match
    triggers = {}
    for channel_inst in config_inst.channels:
        patterns = config_inst.get_aux("triggers")[channel_inst]
        triggers[channel_inst] = [key for key in read_keys if law.util.multi_match(key, patterns)]

    # variable collections
    jet_variables = [key for key in read_keys if key.startswith("Jet_")]
    electron_variables = [key for key in read_keys if key.startswith("Electron_")]
    muon_variables = [key for key in read_keys if key.startswith("Muon_")]

    def select_and_sort_electrons(event):
        # select and sort electrons
        electron_indices = np.where(
            (np.abs(event["Electron_eta"]) < 2.5) &
            (event["Electron_pfRelIso03_all"] < 0.04) &
            (event["Electron_cutBased"] >= 3)
        )[0]
        n_goodElectrons = len(electron_indices)
        electron_indices = electron_indices[np.argsort(-event["Electron_pt"][electron_indices])]

        # apply to all electron variables
        for key in electron_variables:
            event[key] = event[key][electron_indices]
        return (n_goodElectrons, event)

    def select_and_sort_muons(event):
        # select and sort muons
        muon_indices = np.where(
            (np.abs(event["Muon_eta"]) < 2.4) &
            (event["Muon_pfRelIso04_all"] < 0.14) &
            (event["Muon_mediumId"])
        )[0]
        n_goodMuons = len(muon_indices)
        muon_indices = muon_indices[np.argsort(-event["Muon_pt"][muon_indices])]

        # apply to all muons variables
        for key in muon_variables:
            event[key] = event[key][muon_indices]
        return (n_goodMuons, event)

    def select_and_sort_jets(event):
        # select and sort jets
        # we want to build a list of indices that describe a) which jets are selected, and b) how
        # to sort them (e.g. jet_indices = [0, 2, 5, 4])
        # a)
        jet_indices = np.where(
            (np.abs(event["Jet_eta"]) < 2.4) &
            (event["Jet_pt"] > 20.) &
            (event["Jet_jetId"] == 6)
        )[0]
        n_goodJets = len(jet_indices)
        # b)
        jet_indices = jet_indices[np.argsort(-event["Jet_pt"][jet_indices])]
        # apply to all jet variables
        for key in jet_variables:
            event[key] = event[key][jet_indices]
        return (n_goodJets, event)

    @jit(nopython=True)
    def calculate_mll(pt1, pt2, eta1, eta2, phi1, phi2):
        return np.sqrt(2 * pt1 * pt2 * (np.cosh(eta1 - eta2) - np.cos(phi1 - phi2)))

    def select_event(event):

        # run METFilters
        event["METFilters"] = (
            event["Flag_goodVertices"] and event["Flag_globalSuperTightHalo2016Filter"] and
            event["Flag_HBHENoiseFilter"] and event["Flag_HBHENoiseIsoFilter"] and
            event["Flag_HBHENoiseIsoFilter"] and event["Flag_EcalDeadCellTriggerPrimitiveFilter"] and
            event["Flag_BadPFMuonFilter"] and event["Flag_eeBadScFilter"]
        )
        if not event["METFilters"]:
                return False

        n_goodElectrons, electron_event = select_and_sort_electrons(event)
        n_goodMuons, muon_event = select_and_sort_muons(event)
        n_goodJets, jet_event = select_and_sort_jets(event)

        # number of jets selection
        if n_goodJets < 2:
            return False

        # number of b-tags
        n_goodBTags = (jet_event["Jet_btagDeepB"] > 0.4941).sum()
        if n_goodBTags < 2:
            return False

        # lepton cuts that define the channel
        if n_goodElectrons == 2:
            if (
                electron_event["Electron_pt"][0] > 25. and electron_event["Electron_pt"][1] > 15. and
                electron_event["Electron_charge"][0] * electron_event["Electron_charge"][1] == -1
            ):
                channel_inst = config_inst.get_channel("ee")
                mll = calculate_mll(
                    electron_event["Electron_pt"][0],
                    electron_event["Electron_pt"][1],
                    electron_event["Electron_eta"][0],
                    electron_event["Electron_eta"][1],
                    electron_event["Electron_phi"][0],
                    electron_event["Electron_phi"][1],
                )
            else:
                return False
        elif n_goodMuons == 2:
            if (
                muon_event["Muon_pt"][0] > 20. and muon_event["Muon_pt"][1] > 10. and
                muon_event["Muon_charge"][0] * muon_event["Muon_charge"][1] == -1
            ):
                channel_inst = config_inst.get_channel("mumu")
                mll = calculate_mll(
                    electron_event["Muon_pt"][0],
                    electron_event["Muon_pt"][1],
                    electron_event["Muon_eta"][0],
                    electron_event["Muon_eta"][1],
                    electron_event["Muon_phi"][0],
                    electron_event["Muon_phi"][1],
                )
            else:
                return False
        elif (n_goodElectrons >= 1 and n_goodMuons >= 1):
            if (
                electron_event["Electron_pt"][0] > 25. and muon_event["Muon_pt"][0] > 10. and
                electron_event["Electron_charge"][0] * muon_event["Muon_charge"][0] == -1
            ):
                channel_inst = config_inst.get_channel("emu")
                mll = calculate_mll(
                    electron_event["Electron_pt"][0],
                    electron_event["Muon_pt"][0],
                    electron_event["Electron_eta"][0],
                    electron_event["Muon_eta"][0],
                    electron_event["Electron_phi"][0],
                    electron_event["Muon_phi"][0],
                )
            elif (
                muon_event["Muon_pt"][0] > 25. and electron_event["Electron_pt"][0] > 15. and
                muon_event["Muon_charge"][0] * electron_event["Electron_charge"][0] == -1
            ):
                channel_inst = config_inst.get_channel("emu")
                mll = calculate_mll(
                    electron_event["Muon_pt"][0],
                    electron_event["Electron_pt"][0],
                    electron_event["Muon_eta"][0],
                    electron_event["Electron_eta"][0],
                    electron_event["Muon_phi"][0],
                    electron_event["Electron_phi"][0],
                )
            else:
                return False
        else:
            return False

        # check if any of the channel-specific triggers fired
        if not any(event[trigger] for trigger in triggers[channel_inst]):
            return False

        # apply global mll cut
        if mll < 12:
            return False

        # remove events from Z Boson peak
        # 15 GeV window around Z mass: 91.1876 GeV
        if abs(91.1876 - mll) < 15:
            return False

        # store variables
        event["nGoodElectron"] = n_goodElectrons
        event["nGoodMuon"] = n_goodMuons
        event["nGoodJet"] = n_goodJets
        event["nGoodBTag"] = n_goodBTags
        event["mll"] = mll
        event["channelId"] = channel_inst.id

        return True

    return select_event
