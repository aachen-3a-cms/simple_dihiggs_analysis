# coding: utf-8

"""
Physics processes.
If not stated otherwise, cross sections are given in pb.
Values taken from:
- https://twiki.cern.ch/twiki/bin/view/CMS/StandardModelCrossSectionsat13TeVInclusive?rev=18
- https://twiki.cern.ch/twiki/bin/view/CMS/SummaryTable1G25ns?rev=151
"""


import order as od
import scinum as sn

from analysis.config.constants import N_LEPS, BR_WW_DL, BR_WW_SL, BR_H_BB, BR_H_WW


process_diHiggs = od.Process(
    "diHiggs", 1000,
    label="di-Higgs",
    xsecs={
        13: sn.Number(0.03345, {
            "scale": ("rel", 0.043, 0.06),
            "pdf": ("rel", 0.021),
            "alpha_s": ("rel", 0.023),
        }),
    },
)

process_diHiggs_bbWW_dl = process_diHiggs.add_process(
    "diHiggs_bbWW_dl", 1110,
    label=r"$H \rightarrow b\bar{b}$, $H \rightarrow WW$ (DL)",
    xsecs={
        13: process_diHiggs.get_xsec(13) * 2 * BR_H_BB * BR_H_WW * BR_WW_DL,
    },
)

process_tt = od.Process(
    "tt", 2000,
    label=r"$t\bar{t}$ + Jets",
    xsecs={
        13: sn.Number(831.76, {
            "scale": (19.77, 29.20),
            "pdf": 35.06,
            "mtop": (23.18, 22.45),
        }),
    },
)

process_tt_sl = process_tt.add_process(
    "tt_sl", 2100,
    label=r"$t\bar{t}$ + Jets, SL",
    xsecs={
        13: process_tt.get_xsec(13) * BR_WW_SL,
    },
)

process_tt_dl = process_tt.add_process(
    "tt_dl", 2200,
    label=r"$t\bar{t}$ + Jets, DL",
    xsecs={
        13: process_tt.get_xsec(13) * BR_WW_DL,
    },
)

process_dy = od.Process(
    "dy", 3000,
    label="Drell-Yan",
)

process_dy_lep = process_dy.add_process(
    "dy_lep", 3100,
    label=r"Drell-Yan, $Z \rightarrow ll$",
)

process_dy_lep_5To50 = process_dy_lep.add_process(
    "dy_lep_5To50", 3110,
    label=r"Drell-Yan, $5 \leq m_{ll} \leq 50$",
    xsecs={
        13: sn.Number(71310, {
            "scale": 70,
        }),
    },
)

process_dy_lep_10To50 = process_dy_lep.add_process(
    "dy_lep_10To50", 3120,
    label=r"Drell-Yan, $10 \leq m_{ll} \leq 50$",
    xsecs={
        13: sn.Number(18610),
    },
)

process_dy_lep_0j = process_dy_lep.add_process(
    "dy_lep_0j", 3130,
    label=r"Drell-Yan, $0 Jets$",
    xsecs={
        13: sn.Number(4758.9),
    },
)

process_dy_lep_1j = process_dy_lep.add_process(
    "dy_lep_1j", 3140,
    label=r"Drell-Yan, $1 Jets$",
    xsecs={
        13: sn.Number(929.1),
    },
)

process_dy_lep_2j = process_dy_lep.add_process(
    "dy_lep_2j", 3150,
    label=r"Drell-Yan, $2 Jets$",
    xsecs={
        13: sn.Number(337.1),
    },
)

process_dy_lep_50ToInf = process_dy_lep.add_process(
    "dy_lep_50ToInf", 3160,
    label=r"Drell-Yan, $m_{ll} \gt 50$",
    xsecs={
        13: sn.Number(1921.8, {
            "integration": 0.6,
            "pdf": 33.2,
        }) * N_LEPS,
    },
)

process_ww = od.Process(
    "WW", 4100,
    label="WW",
    xsecs={
        13: sn.Number(118.7, {
            "scale": ("rel", 2.5, 2.2),
            "pdf": ("rel", 0.046),
        }),
    },
)

process_ww_sl = process_ww.add_process(
    "WW_sl", 4110,
    label="WW (SL)",
    xsecs={
        13: process_ww.get_xsec(13) * BR_WW_SL,
    },
)

process_ww_dl = process_ww.add_process(
    "WW_dl", 4120,
    label="WW (DL)",
    xsecs={
        13: process_ww.get_xsec(13) * BR_WW_DL,
    },
)

process_zz = od.Process(
    "ZZ", 4200,
    label="ZZ",
)

process_zz_2l_2q = process_zz.add_process(
    "ZZ_2l_2q", 4210,
    label="ZZ (2l2q)",
    xsecs={
        13: sn.Number(3.22),
    },
)

process_zz_2l_2nu = process_zz.add_process(
    "ZZ_2l_2nu", 4220,
    label="ZZ (2l2nu)",
    xsecs={
        13: sn.Number(0.564),
    },
)

process_zz_4l = process_zz.add_process(
    "ZZ_4l", 4230,
    label="ZZ (4l)",
    xsecs={
        13: sn.Number(1.256),
    },
)

process_wz = od.Process(
    "WZ", 4300,
    label="WZ",
)

process_wz_2l_2q = process_wz.add_process(
    "WZ_2l_2q", 4310,
    label="WZ (2l2q)",
    xsecs={
        13: sn.Number(5.595),
    },
)

process_wz_1l_3nu = process_wz.add_process(
    "WZ_1l_3nu", 4320,
    label="WZ (1l3nu)",
    xsecs={
        13: sn.Number(3.033),
    },
)

process_wz_3l_1nu = process_wz.add_process(
    "WZ_3l_1nu", 4330,
    label="WZ (3l1nu)",
    xsecs={
        13: sn.Number(4.42965),
    },
)

process_st = od.Process(
    "st", 5000,
    label="Single top",
)

process_st_t_channel_top = process_st.add_process(
    "st_t_channel_top", 5010,
    label="Single top (t-channel)",
    xsecs={
        13: sn.Number(136.02),
    },
)

process_st_t_channel_antitop = process_st.add_process(
    "st_t_channel_antitop", 5020,
    label="Single anti-top (t-channel)",
    xsecs={
        13: sn.Number(80.95),
    },
)

process_st_s_channel = process_st.add_process(
    "st_s_channel", 5030,
    label="Single top (s-channel)",
    xsecs={
        13: sn.Number(3.36),
    },
)

process_st_tW_top = process_st.add_process(
    "st_tW_top", 5040,
    label="tW",
    xsecs={
        13: sn.Number(19.5545),
    },
)

process_st_tW_antitop = process_st.add_process(
    "st_tW_antitop", 5050,
    label=r"$\overline{t}W$",
    xsecs={
        13: sn.Number(19.5545),
    },
)

process_wjets = od.Process(
    "wjets", 6000,
    label="W+jets",
)

process_wjets_100to200 = process_st.add_process(
    "wjets_100to200", 6010,
    label="W+jets (HT 100-200)",
    xsecs={
        13: sn.Number(1627.45),
    },
)

process_wjets_200to400 = process_st.add_process(
    "wjets_200to400", 6020,
    label="W+jets (HT 200-400)",
    xsecs={
        13: sn.Number(435.237),
    },
)

process_wjets_400to600 = process_st.add_process(
    "wjets_400to600", 6030,
    label="W+jets (HT 400-600)",
    xsecs={
        13: sn.Number(59.181),
    },
)

process_wjets_600to800 = process_st.add_process(
    "wjets_600to800", 6040,
    label="W+jets (HT 600-800)",
    xsecs={
        13: sn.Number(14.58),
    },
)

process_wjets_800to1200 = process_st.add_process(
    "wjets_800to1200", 6050,
    label="W+jets (HT 800-1200)",
    xsecs={
        13: sn.Number(6.656),
    },
)

process_wjets_1200to2500 = process_st.add_process(
    "wjets_1200to2500", 6060,
    label="W+jets (HT 1200-2500)",
    xsecs={
        13: sn.Number(1.608),
    },
)

process_wjets_2500toInf = process_st.add_process(
    "wjets_2500toInf", 6070,
    label="W+jets (HT 2500-Infinity)",
    xsecs={
        13: sn.Number(0.0389),
    },
)

process_ttV = od.Process(
    "ttV", 7000,
    label="ttV",
)

process_ttWjets_2q = process_st.add_process(
    "ttWjets_2q", 7010,
    label="tt W+jets (2q)",
    xsecs={
        13: sn.Number(0.4062),
    },
)

process_ttWjets_1l_2nu = process_st.add_process(
    "ttWjets_1l_2nu", 7020,
    label="tt W+jets (1l 1nu)",
    xsecs={
        13: sn.Number(0.2043),
    },
)

process_ttZ_2q = process_st.add_process(
    "ttZ_2q", 7030,
    label="tt Z (2q)",
    xsecs={
        13: sn.Number(0.5297),
    },
)

process_ttZ_2l_2nu = process_st.add_process(
    "ttZ_2l_2nu", 7040,
    label="tt Z (2l 2nu)",
    xsecs={
        13: sn.Number(0.2529),
    },
)
