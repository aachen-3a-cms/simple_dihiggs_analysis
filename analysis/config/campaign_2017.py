# coding: utf-8
# flake8: noqa

"""
Defintion of the campaign and datasets for CMS in 2018.
"""


import order as od

from analysis.config.processes import *


# campaign
campaign = od.Campaign(
    "Run2_pp_13TeV_2017", 1,
    ecm=13,
    bx=25,
)

# datasets

# Signal
dataset_diHiggs_bbWW_dl = od.Dataset(
    "diHiggs_bbWW_dl", 100,
    campaign=campaign,
    n_files=12,
    keys=[
        "/GluGluToHHTo2B2VTo2L2Nu_node_SM_13TeV-madgraph_correctedcfg/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

# ttbar
dataset_tt_sl = od.Dataset(
    "tt_sl", 200,
    campaign=campaign,
    n_files=106,
    keys=[
        "/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_tt_dl = od.Dataset(
    "tt_dl", 201,
    campaign=campaign,
    n_files=107,
    keys=[
        "/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

# DY
dataset_dy_lep_10To50 = od.Dataset(
    "dy_lep_10To50", 300,
    campaign=campaign,
    n_files=119,
    keys=[
        "/DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
        "/DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14_ext1-v2/NANOAODSIM",
    ],
)

dataset_dy_lep_0j = od.Dataset(
    "dy_lep_0j", 301,
    campaign=campaign,
    n_files=49,
    keys=[
        "/DYJetsToLL_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIIFall17NanoAODv4-PU2017_12Apr2018_Nano14Dec2018_102X_mc2017_realistic_v6-v1/NANOAODSIM",
    ],
)

dataset_dy_lep_1j = od.Dataset(
    "dy_lep_1j", 302,
    campaign=campaign,
    n_files=53,
    keys=[
        "/DYJetsToLL_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIIFall17NanoAODv4-PU2017_12Apr2018_Nano14Dec2018_102X_mc2017_realistic_v6-v1/NANOAODSIM",
    ],
)

dataset_dy_lep_2j = od.Dataset(
    "dy_lep_2j", 303,
    campaign=campaign,
    n_files=126,
    keys=[
        "/DYJetsToLL_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

# Di-Boson VV
# Non-resonant WW
dataset_ww_sl = od.Dataset(
    "ww_sl", 400,
    campaign=campaign,
    n_files=7,
    keys=[
        "/WWToLNuQQ_NNPDF31_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14_ext1-v1/NANOAODSIM",
    ],
)

dataset_ww_dl = od.Dataset(
    "ww_dl", 401,
    campaign=campaign,
    n_files=3,
    keys=[
        "/WWTo2L2Nu_NNPDF31_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14_ext1-v1/NANOAODSIM",
    ],
)

# ZZ
dataset_zz_2l_2q = od.Dataset(
    "zz_2l_2q", 402,
    campaign=campaign,
    n_files=20,
    keys=[
        "/ZZTo2L2Q_13TeV_amcatnloFXFX_madspin_pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_zz_2l_2nu = od.Dataset(
    "zz_2l_2nu", 403,
    campaign=campaign,
    n_files=12,
    keys=[
        "/ZZTo2L2Nu_13TeV_powheg_pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_zz_4l = od.Dataset(
    "zz_4l", 404,
    campaign=campaign,
    n_files=9,
    keys=[
        "/ZZTo4L_13TeV_powheg_pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

# WZ
dataset_wz_2l_2q = od.Dataset(
    "wz_2l_2q", 405,
    campaign=campaign,
    n_files=24,
    keys=[
        "/WZTo2L2Q_13TeV_amcatnloFXFX_madspin_pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_wz_1l_3nu = od.Dataset(
    "wz_1l_3nu", 406,
    campaign=campaign,
    n_files=70,
    keys=[
        "/WZTo1L3Nu_13TeV_amcatnloFXFX_madspin_pythia8_v2/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_wz_3l_1nu = od.Dataset(
    "wz_3l_1nu", 407,
    campaign=campaign,
    n_files=9,
    keys=[
        "/WZTo3LNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

# Single top
# t-channel
dataset_st_t_channel_top = od.Dataset(
    "st_t_channel_top", 500,
    campaign=campaign,
    n_files=36,
    keys=[
        "/ST_t-channel_top_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_st_t_channel_antitop = od.Dataset(
    "st_t_channel_antitop", 501,
    campaign=campaign,
    n_files=11,
    keys=[
        "/ST_t-channel_antitop_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v2/NANOAODSIM",
    ],
)

# s-channel
dataset_st_s_channel = od.Dataset(
    "st_s_channel", 502,
    campaign=campaign,
    n_files=9,
    keys=[
        "/ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v2/NANOAODSIM",
    ],
)

# tW
dataset_st_tW_top = od.Dataset(
    "st_tW_top", 503,
    campaign=campaign,
    n_files=4,
    keys=[
        "/ST_tW_top_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_st_tW_antitop = od.Dataset(
    "st_tW_antitop", 504,
    campaign=campaign,
    n_files=28,
    keys=[
        "/ST_tW_antitop_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v2/NANOAODSIM",
    ],
)

# W+jets
# 100to200 exists only v2
# 2500to exists only v3
# rest is v1
dataset_wjets_100to200 = od.Dataset(
    "wjets_100to200", 600,
    campaign=campaign,
    n_files=267,
    keys=[
        "/WJetsToLNu_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v2/NANOAODSIM",
    ],
)

dataset_wjets_200to400 = od.Dataset(
    "wjets_200to400", 601,
    campaign=campaign,
    n_files=89,
    keys=[
        "/WJetsToLNu_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_wjets_400to600 = od.Dataset(
    "wjets_400to600", 602,
    campaign=campaign,
    n_files=112,
    keys=[
        "/WJetsToLNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_wjets_600to800 = od.Dataset(
    "wjets_600to800", 603,
    campaign=campaign,
    n_files=140,
    keys=[
        "/WJetsToLNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_wjets_800to1200 = od.Dataset(
    "wjets_800to1200", 604,
    campaign=campaign,
    n_files=158,
    keys=[
        "'/WJetsToLNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_wjets_1200to2500 = od.Dataset(
    "wjets_1200to2500", 605,
    campaign=campaign,
    n_files=189,
    keys=[
        "/WJetsToLNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_wjets_2500toInf = od.Dataset(
    "wjets_2500toInf", 606,
    campaign=campaign,
    n_files=209,
    keys=[
        "/WJetsToLNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v3/NANOAODSIM",
    ],
)

# tt+V
dataset_ttWjets_2q = od.Dataset(
    "ttWjets_2q", 700,
    campaign=campaign,
    n_files=2,
    keys=[
        "/TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_ttWjets_1l_2nu = od.Dataset(
    "ttWjets_1l_2nu", 701,
    campaign=campaign,
    n_files=7,
    keys=[
        "/TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_ttZ_2q = od.Dataset(
    "ttZ_2q", 702,
    campaign=campaign,
    n_files=6,
    keys=[
        "/TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)

dataset_ttZ_2l_2nu = od.Dataset(
    "ttZ_2l_2nu", 15886960,
    campaign=campaign,
    n_files=10,
    keys=[
        "/TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8/RunIIFall17NanoAOD-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/NANOAODSIM",
    ],
)



# link processes to datasets
dataset_diHiggs_bbWW_dl.add_process(process_diHiggs_bbWW_dl)
dataset_tt_sl.add_process(process_tt_sl)
dataset_tt_dl.add_process(process_tt_dl)
dataset_dy_lep_10To50.add_process(process_dy_lep_10To50)
dataset_dy_lep_0j.add_process(process_dy_lep_0j)
dataset_dy_lep_1j.add_process(process_dy_lep_1j)
dataset_dy_lep_2j.add_process(process_dy_lep_2j)
dataset_ww_sl.add_process(process_ww_sl)
dataset_ww_dl.add_process(process_ww_dl)
dataset_zz_2l_2q.add_process(process_zz_2l_2q)
dataset_zz_2l_2nu.add_process(process_zz_2l_2nu)
dataset_zz_4l.add_process(process_zz_4l)
dataset_wz_2l_2q.add_process(process_wz_2l_2q)
dataset_wz_1l_3nu.add_process(process_wz_1l_3nu)
dataset_wz_3l_1nu.add_process(process_wz_3l_1nu)
dataset_st_t_channel_top.add_process(process_st_t_channel_top)
dataset_st_t_channel_antitop.add_process(process_st_t_channel_antitop)
dataset_st_s_channel.add_process(process_st_s_channel)
dataset_st_tW_top.add_process(process_st_tW_top)
dataset_st_tW_antitop.add_process(process_st_tW_antitop)
dataset_wjets_100to200.add_process(process_wjets_100to200)
dataset_wjets_200to400.add_process(process_wjets_200to400)
dataset_wjets_400to600.add_process(process_wjets_400to600)
dataset_wjets_600to800.add_process(process_wjets_600to800)
dataset_wjets_800to1200.add_process(process_wjets_800to1200)
dataset_wjets_1200to2500.add_process(process_wjets_1200to2500)
dataset_wjets_2500toInf.add_process(process_wjets_2500toInf)
dataset_ttWjets_2q.add_process(process_ttWjets_2q)
dataset_ttWjets_1l_2nu.add_process(process_ttWjets_1l_2nu)
dataset_ttZ_2q.add_process(process_ttZ_2q)
dataset_ttZ_2l_2nu.add_process(process_ttZ_2l_2nu)
