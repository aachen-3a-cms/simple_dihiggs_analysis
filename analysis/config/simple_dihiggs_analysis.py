# coding: utf-8
# flake8: noqa

"""
Definition of the simple diHiggs analysis in the bbWW (DL) channel.
"""

import scinum as sn
import order as od

from analysis.config.processes import (
    process_diHiggs_bbWW_dl, process_tt_sl, process_tt_dl, process_ww_sl, process_ww_dl,
    process_dy_lep_10To50, process_dy_lep_0j, process_dy_lep_0j, process_dy_lep_1j,
    process_dy_lep_2j, process_zz_2l_2q, process_zz_2l_2nu, process_zz_4l, process_wz_2l_2q,
    process_wz_1l_3nu, process_wz_3l_1nu, process_st_t_channel_top, process_st_t_channel_antitop,
    process_st_s_channel, process_st_tW_top, process_st_tW_antitop, process_wjets_100to200,
    process_wjets_200to400, process_wjets_400to600, process_wjets_600to800, process_wjets_800to1200,
    process_wjets_1200to2500, process_wjets_2500toInf, process_ttWjets_2q, process_ttWjets_1l_2nu,
    process_ttZ_2q, process_ttZ_2l_2nu,
)


#
# analysis and config
#

from analysis.config.campaign_2017 import campaign as campaign_2017

# create the analysis
analysis = od.Analysis("sdha", 1)

# setup the config for the 2017 campaign
# we pass no name, so the config will have the same name as the campaign
config_2017 = cfg = analysis.add_config(campaign=campaign_2017)


#
# add processes and datasets
#

# link processes that are considered
cfg.add_process(process_diHiggs_bbWW_dl)
cfg.add_process(process_tt_sl)
cfg.add_process(process_tt_dl)
cfg.add_process(process_dy_lep_10To50)
cfg.add_process(process_dy_lep_0j)
cfg.add_process(process_dy_lep_1j)
cfg.add_process(process_dy_lep_2j)
cfg.add_process(process_ww_sl)
cfg.add_process(process_ww_dl)
cfg.add_process(process_zz_2l_2q)
cfg.add_process(process_zz_2l_2nu)
cfg.add_process(process_zz_4l)
cfg.add_process(process_wz_2l_2q)
cfg.add_process(process_wz_1l_3nu)
cfg.add_process(process_wz_3l_1nu)
cfg.add_process(process_st_t_channel_top)
cfg.add_process(process_st_t_channel_antitop)
cfg.add_process(process_st_s_channel)
cfg.add_process(process_st_tW_top)
cfg.add_process(process_st_tW_antitop)
cfg.add_process(process_wjets_100to200)
cfg.add_process(process_wjets_200to400)
cfg.add_process(process_wjets_400to600)
cfg.add_process(process_wjets_600to800)
cfg.add_process(process_wjets_800to1200)
cfg.add_process(process_wjets_1200to2500)
cfg.add_process(process_wjets_2500toInf)
cfg.add_process(process_ttWjets_2q)
cfg.add_process(process_ttWjets_1l_2nu)
cfg.add_process(process_ttZ_2q)
cfg.add_process(process_ttZ_2l_2nu)

# add datasets
dataset_names = [
    "diHiggs_bbWW_dl",
    "tt_sl", "tt_dl",
    "dy_lep_10To50", "dy_lep_0j", "dy_lep_1j", "dy_lep_2j",
    "ww_sl", "ww_dl",
    "zz_2l_2q", "zz_2l_2nu", "zz_4l",
    "wz_2l_2q", "wz_1l_3nu", "wz_3l_1nu",
    "st_t_channel_top", "st_t_channel_antitop", "st_s_channel", "st_tW_top", "st_tW_antitop",
    "wjets_100to200", "wjets_200to400", "wjets_400to600", "wjets_600to800", "wjets_800to1200", "wjets_1200to2500", "wjets_2500toInf",
    "ttWjets_2q", "ttWjets_1l_2nu", "ttZ_2q", "ttZ_2l_2nu",
]

for dataset_name in dataset_names:
    dataset = campaign_2017.get_dataset(dataset_name)
    cfg.add_dataset(dataset)


#
# channels
#

ch_ee = cfg.add_channel("ee", 0)
ch_emu = cfg.add_channel("emu", 1)
ch_mumu = cfg.add_channel("mumu", 2)


#
# variables for histograms
#

# number of jets
cfg.add_variable(
    name="nGoodJet",
    expression="nGoodJet",
    binning=(10, 0., 10.),
    x_title="N(jets)",
)
cfg.add_variable(
    name="nGoodBTag",
    expression="nGoodBTag",
    binning=(10, 0., 10.),
    x_title="N(b-tagged jets)",
)

# pt and eta of the 2 leptons
for i in range(1, 2 + 1):
    cfg.add_variable(
        name="lep{}_pt".format(i),
        expression="(lep{0}_px**2 + lep{0}_py**2)**0.5".format(i),
        binning=(25, 0., 500.),
        unit="GeV",
        x_title="Lepton {} p_{{T}}".format(i),
    )
    cfg.add_variable(
        name="lep{}_eta".format(i),
        binning=(25, -2.5, 2.5),
        x_title=r"Lepton {} \eta".format(i),
    )

# pt and eta of the first 4 jets
for i in range(1, 4 + 1):
    cfg.add_variable(
        name="jet{}_pt".format(i),
        expression="(jet{0}_px**2 + jet{0}_py**2)**0.5".format(i),
        binning=(25, 0., 500.),
        unit="GeV",
        x_title="Jet {} p_{{T}}".format(i),
    )
    cfg.add_variable(
        name="jet{}_eta".format(i),
        binning=(25, -2.5, 2.5),
        x_title=r"Jet {} \eta".format(i),
    )


#
# other configurations
#

# electron SF
# https://twiki.cern.ch/twiki/bin/viewauth/CMS/EgammaRunIIRecommendations#Electron_Scale_Factors
cfg.set_aux("electronSF", {
    "reco_lowpt": ["analysis/recipes/electronSF/egammaEffi.txt_EGM2D_runBCDEF_passingRECO_lowEt.root", "EGamma_SF2D"],
    "reco_highpt": ["analysis/recipes/electronSF/egammaEffi.txt_EGM2D_runBCDEF_passingRECO.root", "EGamma_SF2D"],
    "ID": ["analysis/recipes/electronSF/2017_ElectronMedium.root", "EGamma_SF2D"],
})

# muon SF
# https://twiki.cern.ch/twiki/bin/viewauth/CMS/MuonReferenceEffs2017
cfg.set_aux("muonSF", {
    "ISO": {
        "file": "analysis/recipes/muonSF/RunBCDEF_SF_ISO_syst.root",
        "sf": "NUM_TightRelIso_DEN_MediumID_pt_abseta",
        "syst": "NUM_TightRelIso_DEN_MediumID_pt_abseta_syst",
        "stat": "NUM_TightRelIso_DEN_MediumID_pt_abseta_stat",
    },
    "ID": {
        "file": "analysis/recipes/muonSF/RunBCDEF_SF_ID_syst.root",
        "sf": "NUM_MediumID_DEN_genTracks_pt_abseta",
        "syst": "NUM_MediumID_DEN_genTracks_pt_abseta_syst",
        "stat": "NUM_MediumID_DEN_genTracks_pt_abseta_stat",
    },
})


# pileup for 2017
# https://hypernews.cern.ch/HyperNews/CMS/get/physics-validation/2995/1.html
# https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/SimGeneral/MixingModule/python/mix_2017_25ns_WinterMC_PUScenarioV1_PoissonOOTPU_cfi.py
cfg.set_aux("pileup", {
    "pileup_file": "analysis/recipes/pileup/pileup_latest.txt",
    "pileup_mc": [
        3.39597497605e-05, 6.63688402133e-06, 1.39533611284e-05, 3.64963078209e-05, 6.00872171664e-05,
        9.33932578027e-05, 0.000120591524486, 0.000128694546198, 0.000361697233219, 0.000361796847553,
        0.000702474896113, 0.00133766053707, 0.00237817050805, 0.00389825605651, 0.00594546732588,
        0.00856825906255, 0.0116627396044, 0.0148793350787, 0.0179897368379, 0.0208723871946,
        0.0232564170641, 0.0249826433945, 0.0262245860346, 0.0272704617569, 0.0283301107549,
        0.0294006137386, 0.0303026836965, 0.0309692426278, 0.0308818046328, 0.0310566806228,
        0.0309692426278, 0.0310566806228, 0.0310566806228, 0.0310566806228, 0.0307696426944,
        0.0300103336052, 0.0288355370103, 0.0273233309106, 0.0264343533951, 0.0255453758796,
        0.0235877272306, 0.0215627588047, 0.0195825559393, 0.0177296309658, 0.0160560731931,
        0.0146022004183, 0.0134080690078, 0.0129586991411, 0.0125093292745, 0.0124360740539, 0.0123547104433,
        0.0123953922486, 0.0124360740539, 0.0124360740539, 0.0123547104433, 0.0124360740539, 0.0123387597772,
        0.0122414455005, 0.011705203844, 0.0108187105305, 0.00963985508986, 0.00827210065136, 0.00683770076341,
        0.00545237697118, 0.00420456901556, 0.00367513566191, 0.00314570230825, 0.0022917978982,
        0.00163221454973, 0.00114065309494, 0.000784838366118, 0.000533204105387, 0.000358474034915,
        0.000238881117601, 0.0001984254989, 0.000157969880198, 0.00010375646169, 6.77366175538e-05,
        4.39850477645e-05, 2.84298066026e-05, 1.83041729561e-05, 1.17473542058e-05, 7.51982735129e-06,
        6.16160108867e-06, 4.80337482605e-06, 3.06235473369e-06, 1.94863396999e-06, 1.23726800704e-06,
        7.83538083774e-07, 4.94602064224e-07, 3.10989480331e-07, 1.94628487765e-07, 1.57888581037e-07,
        1.2114867431e-07, 7.49518929908e-08, 4.6060444984e-08, 2.81008884326e-08, 1.70121486128e-08,
        1.02159894812e-08,
    ],
    "min_bias_xsec": sn.Number(69200, {"minbiasxs": ("rel", 0.046)}),
})

# luminosities per channel in /pb
# https://hypernews.cern.ch/HyperNews/CMS/get/luminosity/749.html
cfg.set_aux("lumi", {
    "lumi_file": "analysis/recipes/pileup/Cert_294927-306462_13TeV_EOY2017ReReco_Collisions17_JSON.txt",
    ch_ee: 41296.082,
    ch_emu: 41296.082,
    ch_mumu: 41296.082,
})

# triggers per channel
cfg.set_aux("triggers", {
    ch_ee: [
        "HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL",
        "HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ",
    ],
    ch_emu: [
        "HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL",
        "HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ",
        "HLT_Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ",
        "HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ",
    ],
    ch_mumu: [
        "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ",
        "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8",
        "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8",
    ],
})


#
# versions
#

cfg.set_aux("versions", {
    "sdha.Selection": "dev1",
    "sdha.Selection": "dev2"
})
