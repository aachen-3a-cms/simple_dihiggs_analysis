# coding: utf-8


import subprocess

import law

from analysis.tasks.base import DatasetTask, HTCondorWorkflow


class GetDatasetLFNs(DatasetTask, law.TransferLocalFile):

    source_path = None
    version = None

    def single_output(self):
        h = law.util.create_hash(self.dataset_inst.keys)
        return self.wlcg_target("lfns_{}.json".format(h))

    def run(self):
        lfns = []
        for key in self.dataset_inst.keys:
            print("get lfns for key {}".format(key))
            cmd = "dasgoclient -query='file dataset={}' -limit=0".format(key)
            code, out, _ = law.util.interruptable_popen(cmd, shell=True, stdout=subprocess.PIPE,
                                                        executable="/bin/bash")
            if code != 0:
                raise Exception("dasgoclient query failed")
            lfns.extend(out.strip().split("\n"))

        self.publish_message("found {} lfns".format(len(lfns)))

        if len(lfns) != self.dataset_inst.n_files:
            raise ValueError("Number of lfns does not match number of files "
                             "for dataset {}".format(self.dataset_inst.name))

        tmp = law.LocalFileTarget(is_tmp="json")
        tmp.dump(lfns)
        self.transfer(tmp)


class DownloadDataset(DatasetTask, HTCondorWorkflow, law.LocalWorkflow):

    version = None

    def workflow_requires(self):
        reqs = self.workflow_cls.workflow_requires(self)

        if self.workflow == "local" or not self.is_controlling_remote_jobs():
            reqs.update(self.requires_from_branch())

        return reqs

    def requires(self):
        return {
            "lfns": GetDatasetLFNs.req(self, replicas=10),
        }

    def output(self):
        return {
            "data": self.wlcg_target("{}.root".format(self.branch)),
        }

    @law.decorator.timeit(publish_message=True)
    def run(self):
        # load the lfn for our branch
        lfn = self.input()["lfns"].random_target().load()[self.branch_data]

        # determine the xrd redirector and download the file
        redirector = "xrootd-cms.infn.it"
        xrd_url = "root://{}/{}".format(redirector, lfn)

        with self.output()["data"].localize("w") as tmp:
            # needs `xrdcp-old` for some reason...
            # xrdcp will lead to a symbol lookup error
            cmd = "xrdcp-old {} {}".format(xrd_url, tmp.path)
            self.publish_message(
                "Downloading file from {} ...".format(xrd_url))
            code = law.util.interruptable_popen(
                cmd, shell=True, executable="/bin/bash")[0]
            if code != 0:
                raise Exception("download failed")
