# coding: utf-8

from collections import OrderedDict
import law
import os

from analysis.tasks.base import AnalysisTask, DatasetTask, HTCondorWorkflow
from analysis.tasks.external import DownloadDataset

from analysis.recipes.corrections import Weighter, electron_scale_factors, roccor_scale_factors, muon_scale_factors


class PileUpReweighting(AnalysisTask):

    def output(self):
        return {
            "pileup": self.wlcg_target("pileup_weights.root"),
        }

    @law.decorator.timeit(publish_message=True)
    def run(self):
        import ROOT
        ROOT.PyConfig.IgnoreCommandLineOptions = True
        ROOT.gROOT.SetBatch()

        with self.output()["pileup"].localize("w") as output:

            output_path = output.path
            pileup_file = self.config_inst.get_aux("pileup")["pileup_file"]
            pileup_mc = self.config_inst.get_aux("pileup")["pileup_mc"]
            num_pileup_bins = len(pileup_mc)

            # only nominal for now
            min_bias_xsec = self.config_inst.get_aux(
                "pileup")["min_bias_xsec"].nominal

            lumi_file = self.config_inst.get_aux("lumi")["lumi_file"]
            cmd = "pileupCalc.py -i {lumi_file} --inputLumiJSON {pileup_file} --calcMode true " \
                "--minBiasXsec {min_bias_xsec} --maxPileupBin {num_pileup_bins} " \
                "--numPileupBins {num_pileup_bins} {output_path}".format(
                    **locals())

            code = law.util.interruptable_popen(
                cmd, shell=True, executable="/bin/bash")[0]

            # get data and MC pileup
            with output.load("UPDATE", formatter="root") as output_tmp:
                pileup_hist_data = output_tmp.Get(
                    "pileup").Clone("pileup_data")
                pileup_hist_mc = ROOT.TH1D(
                    "pileup_mc", "", num_pileup_bins, 0., num_pileup_bins)
                for i, prob in enumerate(pileup_mc):
                    # MC: fill bin and set error to 0 (TH1.Fill returns the bin number)
                    pileup_hist_mc.SetBinError(
                        pileup_hist_mc.Fill(i, prob), 0.)
                pileup_hist_data.Scale(1. / pileup_hist_data.Integral())
                pileup_hist_mc.Scale(1. / pileup_hist_mc.Integral())
                pileup_hist_data.Write()
                pileup_hist_mc.Write()

                # create weight histogram
                weight_hist = pileup_hist_data.Clone("pileup_weights")
                weight_hist.SetTitle("pileup weights")
                weight_hist.Divide(pileup_hist_mc)
                weight_hist.Write()

                ROOT.gDirectory.Delete("pileup;*")
                output_tmp.Close()

                # dump output
                output.dump()


class CreateCorrections(DatasetTask, HTCondorWorkflow, law.LocalWorkflow):

    def requires(self):
        return DownloadDataset.req(self)

    def output(self):
        out = {
            "Rochester_sf": self.wlcg_target("Rochester_sf_{}.npz".format(self.branch)),
        }
        if not self.config_inst.get_dataset(self.dataset).is_data:
            out.update({
                "Electron_sf": self.wlcg_target("Electron_sf_{}.npz".format(self.branch)),
                "Muon_sf": self.wlcg_target("Muon_sf_{}.npz".format(self.branch)),
            })
        return out

    @law.decorator.notify
    def run(self):
        import ROOT
        import awkward

        # initialie Muon weighters and Rochester Correction
        roccor_path = os.getenv("SDHA_BASE") + "/analysis/recipes/rochester/"
        self.publish_message("Loading RoccoR from {}".format(roccor_path))
        ROOT.gROOT.ProcessLine("#include <{}RoccoR.cc>".format(roccor_path))
        is_data = self.config_inst.get_dataset(self.dataset).is_data
        roccor = ROOT.RoccoR("analysis/recipes/rochester/RoccoR2017.txt")

        # initialise electron weighters
        ### RECO ###
        # pt < 20 GeV
        electron_reco_lowpt = self.config_inst.get_aux("electronSF")["reco_lowpt"]
        electron_reco_lowpt_weighter = Weighter(electron_reco_lowpt[0], electron_reco_lowpt[1])
        electron_reco_lowpt_weighter.fillOverflow2D()

        # pt > 20 GeV
        electron_reco_highpt = self.config_inst.get_aux("electronSF")["reco_highpt"]
        electron_reco_highpt_weighter = Weighter(electron_reco_highpt[0], electron_reco_highpt[1])
        electron_reco_highpt_weighter.fillOverflow2D()

        ### ID ###
        electron_id = self.config_inst.get_aux("electronSF")["ID"]
        electron_id_weighter = Weighter(electron_id[0], electron_id[1])
        electron_id_weighter.fillOverflow2D()

        # initialise muon weighters
        ### ISO ###
        muon_iso = self.config_inst.get_aux("muonSF")["ISO"]
        muon_iso_weighter = Weighter(muon_iso["file"], muon_iso["sf"])
        muon_iso_weighter.fillOverflow2D()
        muon_iso_weighter_syst = Weighter(muon_iso["file"], muon_iso["syst"])
        muon_iso_weighter_syst.fillOverflow2D()
        muon_iso_weighter_stat = Weighter(muon_iso["file"], muon_iso["stat"])
        muon_iso_weighter_stat.fillOverflow2D()

        ### ID ###
        muon_id = self.config_inst.get_aux("muonSF")["ID"]
        muon_id_weighter = Weighter(muon_id["file"], muon_id["sf"])
        muon_id_weighter.fillOverflow2D()
        muon_id_weighter_syst = Weighter(muon_id["file"], muon_id["syst"])
        muon_id_weighter_syst.fillOverflow2D()
        muon_id_weighter_stat = Weighter(muon_id["file"], muon_id["stat"])
        muon_id_weighter_stat.fillOverflow2D()

        with self.input()["data"].load("READ", formatter="root") as inp:
            tree = inp.Get("Events")

            # speed up loop
            tree.SetBranchStatus("*", 0)

            # muon branches
            tree.SetBranchStatus("nMuon", 1)
            tree.SetBranchStatus("Muon_pt", 1)
            tree.SetBranchStatus("Muon_charge", 1)
            tree.SetBranchStatus("Muon_pt", 1)
            tree.SetBranchStatus("Muon_eta", 1)
            tree.SetBranchStatus("Muon_phi", 1)
            tree.SetBranchStatus("Muon_nTrackerLayers", 1)

            if not is_data:
                # GenParticle branches
                tree.SetBranchStatus("Muon_genPartIdx", 1)
                tree.SetBranchStatus("GenPart_pt", 1)
                tree.SetBranchStatus("GenPart_pdgId", 1)

            # electron branches
            tree.SetBranchStatus("nElectron", 1)
            tree.SetBranchStatus("Electron_pt", 1)
            tree.SetBranchStatus("Electron_eta", 1)

            SF = OrderedDict([
                ("Rochester_sf", []),
                ("Electron_sf", []),
                ("Muon_sf", []),
            ])

            # eventloop
            for event in tree:
                roccor_sf = roccor_scale_factors(
                    event,
                    is_data,
                    roccor,
                )
                if not is_data:
                    electron_sf = electron_scale_factors(
                        event,
                        electron_reco_lowpt_weighter,
                        electron_reco_highpt_weighter,
                        electron_id_weighter,
                    )
                    muon_sf = muon_scale_factors(
                        event,
                        muon_iso_weighter,
                        muon_id_weighter,
                    )
                SF["Rochester_sf"].append(roccor_sf)
                if not is_data:
                    SF["Electron_sf"].append(electron_sf)
                    SF["Muon_sf"].append(muon_sf)

            self.output()["Rochester_sf"].dump(Rochester_sf=awkward.fromiter(SF["Rochester_sf"]))
            self.publish_message("{} has been created".format(self.output()["Rochester_sf"].basename))
            if not is_data:
                self.output()["Electron_sf"].dump(Electron_sf=awkward.fromiter(SF["Electron_sf"]))
                self.publish_message("{} has been created".format(self.output()["Electron_sf"].basename))
                self.output()["Muon_sf"].dump(Muon_sf=awkward.fromiter(SF["Muon_sf"]))
                self.publish_message("{} has been created".format(self.output()["Muon_sf"].basename))


class CreateCorrectionsWrapper(AnalysisTask, law.WrapperTask):

    def requires(self):
        return [
            CreateCorrections.req(self, dataset=dataset.name)
            for dataset in self.config_inst.datasets
        ]
