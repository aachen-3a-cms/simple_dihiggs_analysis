# coding: utf-8


from collections import OrderedDict

import law
import six

from analysis.tasks.base import AnalysisTask, DatasetTask, HTCondorWorkflow
from analysis.tasks.external import DownloadDataset
from analysis.tasks.corrections import CreateCorrections


class Selection(DatasetTask, HTCondorWorkflow, law.LocalWorkflow):

    def workflow_requires(self):
        reqs = self.workflow_cls.workflow_requires(self)

        if self.workflow == "local" or not self.is_controlling_remote_jobs():
            reqs.update(self.requires_from_branch())

        return reqs

    def requires(self):
        return [DownloadDataset.req(self), CreateCorrections.req(self)]

    def output(self):
        return {
            "data": self.wlcg_target("data_{}.npz".format(self.branch)),
        }

    @law.decorator.timeit(publish_message=True)
    def run(self):
        import numpy as np

        # load datastructure and cuts
        from analysis.recipes.selection import build_selection_struct, build_event_selection

        # helper for better, chunked uproot iteration
        def uproot_iter(tree, branches, **kwargs):
            for start, stop, chunk in tree.iterate(branches, reportentries=True, **kwargs):
                for i in six.moves.range(stop - start):
                    yield {key: arr[i] for key, arr in six.iteritems(chunk)}

        # data structure definition for the output file
        struct = build_selection_struct(self.config_inst)

        # load the input file
        with self.input()["data"].load(formatter="uproot") as inp:
            tree = inp["Events"]

            # law progress callback
            progress = self.create_progress_callback(len(tree))

            # determine all keys we need to read, consider globbing
            read_patterns = sum((v.get("requires", []) for v in six.itervalues(struct)), [])
            read_patterns = list(set(read_patterns))
            read_keys = [key for key in tree.keys() if law.util.multi_match(key, read_patterns)]

            self.publish_message("loading {} branches for {} events".format(
                len(read_keys), len(tree)))

            # event selection function
            select_event = build_event_selection(self.config_inst, struct, read_keys)

            # data storage for conversion to record array
            data = OrderedDict([(key, []) for key, value in six.iteritems(struct)])

            for i, event in enumerate(uproot_iter(tree, read_keys)):
                # run the event selection and, if successful, write event variables to data
                if select_event(event):
                    for key, obj in six.iteritems(struct):
                        value = event
                        for idx in obj["selector"]:
                            try:
                                value = value[idx]
                            except IndexError:
                                value = -1e5
                                break
                        data[key].append(value)

                progress(i)

        # convert list of records to record array
        with self.publish_step("saving record array"):
            dtype = [(key, struct[key]["dtype"]) for key, value in six.iteritems(struct)]
            rec = np.core.records.fromarrays(data.values(), dtype=dtype)
            self.publish_message("selected {} events".format(len(rec)))
            self.output()["data"].dump(data=rec)


class SelectionWrapper(AnalysisTask, law.WrapperTask):

    def requires(self):
        return [Selection.req(self, dataset=dataset.name) for dataset in self.config_inst.datasets]


class MergeDataset(DatasetTask, law.CascadeMerge):

    merge_factor = 4

    def create_branch_map(self):
        return law.CascadeMerge.create_branch_map(self)

    def cascade_workflow_requires(self, **kwargs):
        return Selection.req(self, version=self.get_version(Selection),
            _exclude=("start_branch", "end_branch"), **kwargs)

    def cascade_requires(self, start_leaf, end_leaf):
        return [self.cascade_workflow_requires(branch=l) for l in range(start_leaf, end_leaf)]

    def trace_cascade_inputs(self, inputs):
        return [inp["data"] for inp in inputs]

    def cascade_output(self):
        return law.SiblingFileCollection([
            self.wlcg_target("merged_{}.npz".format(self.dataset))
        ])

    def merge(self, inputs, output):
        import numpy as np
        with self.publish_step("merging ..."):
            seq = tuple(input.load(formatter="numpy")["data"] for input in inputs)
            merged = np.hstack(seq)
            self.output().dump(data=merged)


class MergeDatasetWrapper(AnalysisTask, law.WrapperTask):

    def requires(self):
        return [
            MergeDataset.req(self, dataset=dataset.name)
            for dataset in self.config_inst.datasets
        ]
