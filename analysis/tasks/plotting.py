from collections import OrderedDict

import luigi
import law
import six

from analysis.tasks.base import AnalysisTask, DatasetTask
from analysis.tasks.selection import MergeDataset
from analysis.tasks.external import GetDatasetLFNs


class CountGeneratedEvents(DatasetTask):

    def requires(self):
        return {
            "lfns": GetDatasetLFNs.req(self, replicas=10),
        }

    def output(self):
        return {
            "nGenEvents": self.local_target("baseW_{}.json".format(self.dataset)),
        }

    def run(self):
        lfns = self.input()["lfns"].random_target().load()
        output = self.output()["nGenEvents"]
        output.parent.touch()

        nGenEvents = 0.0
        for lfn in lfns:
            with law.WLCGFileTarget(lfn, fs="cms_fs").load(formatter="uproot") as inp:
                tree = inp["Events"]
                nGenEvents += tree.array("Generator_weight").sum()
        self.publish_message(
            "Number of generated Events in " \
            "{}: {}".format(self.dataset, nGenEvents)
        )
        output.dump(nGenEvents)


class CountGeneratedEventsWrapper(AnalysisTask, law.WrapperTask):

    def requires(self):
        return [
            CountGeneratedEvents.req(self, dataset=dataset.name)
            for dataset in self.config_inst.datasets
        ]


class StackedMC(AnalysisTask):

    channel = luigi.Parameter(default="ee")
    variable = luigi.Parameter(default="nGoodJet")
    normalize = luigi.BoolParameter()
    logy = luigi.BoolParameter()

    def requires(self):
        return {
            dataset.name: (
                MergeDataset.req(self, dataset=dataset.name),
                CountGeneratedEvents.req(self, dataset=dataset.name)
            )
            for dataset in self.config_inst.datasets
        }

    def output(self):
        if not self.normalize:
            return self.local_target("{}_{}.pdf".format(self.variable, self.channel))
        else:
            return self.local_target("{}_{}_normalized.pdf".format(self.variable, self.channel))

    def run(self):
        import numpy as np
        import matplotlib as mpl
        mpl.use("Agg")
        import matplotlib.pyplot as plt

        inputs = self.input()
        output = self.output()
        output.parent.touch()

        variable = self.config_inst.get_variable(self.variable)
        channel = self.config_inst.get_channel(self.channel)

        # load and order merged numpy arrays
        hists = OrderedDict([
            (key, value[0]["collection"].targets[0].load(formatter="numpy")["data"])
            for key, value in six.iteritems(inputs)
        ])

        # build sequence of numpy arrays for plt.hist && select channel {default: "ee"}
        hist_seq = [
            hists[key][np.where(hists[key]["channelId"] == channel.id)][variable.name]
            for key in six.iterkeys(hists)
        ]

        # weight each process with 'xsec * lumi / # generated events'
        # get xsecs:
        xsecs = OrderedDict([
            (key, self.config_inst.get_process(key).get_xsec(13).nominal)
            for key in six.iterkeys(inputs)
        ])

        # luminosities
        lumis = self.config_inst.get_aux("lumi").values()
        lumi = lumis[channel.id]

        # get number of generated events
        nGenEvents = OrderedDict([
            (key, value[1]["nGenEvents"].load())
            for key, value in six.iteritems(inputs)
        ])

        # calculate weight for each process
        weights = OrderedDict([
            (key, lumi * xsecs[key] / nGenEvents[key])
            for key in six.iterkeys(inputs)
        ])

        weights_seq = [
            weights[key] * hists[key][
                np.where(
                    hists[key]["channelId"] == channel.id
                )
            ]["Generator_weight"]
            for key in six.iterkeys(hists)
        ]

        # get process labels for legend
        process_labels = [
            self.config_inst.get_process(key).label
            for key in six.iterkeys(inputs)
        ]

        # finally plotting...
        plt.hist(
            hist_seq, bins=variable.bin_edges, histtype="stepfilled",
            weights=weights_seq, normed=self.normalize,
            label=process_labels, stacked=True
        )
        plt.xlabel(variable.full_x_title())
        plt.ylabel(variable.full_y_title())
        plt.title(
            "CMS Private Work",
            loc="right",
            style="italic"
        )
        plt.title(
            r'$\mathcal{{L}}={0:.1f}\mathrm{{fb}}^\mathrm{{-1}}' \
            r'\,(2017,\,13\mathrm{{TeV}})$'.format(lumi / 1000.),
            loc="left"
        )
        plt.legend()
        if self.logy:
            plt.yscale('log')
        plt.savefig(output.path)
