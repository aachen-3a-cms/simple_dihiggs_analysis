# coding: utf-8


__all__ = ["AnalysisTask", "DatasetTask", "GridWorkflow"]


import re
import os

import luigi
import six
import law

law.contrib.load("cms", "git", "glite", "numpy", "tasks", "root", "slack", "telegram", "wlcg", "htcondor")

from analysis.config.simple_dihiggs_analysis import analysis


class AnalysisTask(law.Task):

    task_namespace = "sdha"

    version = luigi.Parameter(description="task version")
    notify_slack = law.NotifySlackParameter()
    notify_telegram = law.NotifyTelegramParameter()

    outputs_siblings = True

    workflow_run_decorators = [law.decorator.notify]

    message_cache_size = 20

    exclude_params_req = {"notify"}

    # the campaign is hardcoded for the moment
    campaign = "Run2_pp_13TeV_2017"

    def __init__(self, *args, **kwargs):
        super(AnalysisTask, self).__init__(*args, **kwargs)

        self.analysis_inst = analysis
        self.config_inst = self.analysis_inst.get_config(self.campaign)

    def get_version(self, task_cls):
        family = task_cls if isinstance(task_cls, six.string_types) else task_cls.get_task_family()
        return self.config_inst.get_aux("versions")[family]

    def store_parts(self):
        parts = (self.__class__.__name__, self.config_inst.name)
        if self.version is not None:
            parts += (self.version,)
        return parts

    def local_path(self, *path):
        parts = [str(p) for p in self.store_parts() + path]
        return os.path.join(os.environ["SDHA_STORE"], *parts)

    def wlcg_path(self, *path):
        parts = [str(p) for p in self.store_parts() + path]
        return os.path.join(*parts)

    def local_target(self, *args):
        cls = law.LocalFileTarget if args else law.LocalDirectoryTarget
        return cls(self.local_path(*args))

    def wlcg_target(self, *args, **kwargs):
        cls = law.WLCGFileTarget if args else law.WLCGDirectoryTarget
        return cls(self.wlcg_path(*args), **kwargs)


class DatasetTask(AnalysisTask):

    dataset = luigi.Parameter(default="diHiggs_bbWW_dl")

    def __init__(self, *args, **kwargs):
        super(DatasetTask, self).__init__(*args, **kwargs)

        self.dataset_inst = self.config_inst.get_dataset(self.dataset)

    def store_parts(self):
        return super(DatasetTask, self).store_parts() + (self.dataset,)

    def create_branch_map(self):
        return {i: i for i in range(self.dataset_inst.n_files)}


class GridWorkflow(AnalysisTask, law.GLiteWorkflow):

    glite_ce_map = {
        "RWTH": "grid-ce.physik.rwth-aachen.de:8443/cream-pbs-cms",
        "RWTH_short": "grid-ce.physik.rwth-aachen.de:8443/cream-pbs-short",
        "CNAF": [
            "ce04-lcg.cr.cnaf.infn.it:8443/cream-lsf-cms",
            "ce05-lcg.cr.cnaf.infn.it:8443/cream-lsf-cms",
            "ce06-lcg.cr.cnaf.infn.it:8443/cream-lsf-cms",
        ],
        "IRFU": "node74.datagrid.cea.fr:8443/cream-pbs-cms",
        "IIHE": "cream02.iihe.ac.be:8443/cream-pbs-cms",
        "CIEMAT": [
            "creamce02.ciemat.es:8443/cream-pbs-medium",
            "creamce03.ciemat.es:8443/cream-pbs-medium",
        ],
    }

    grid_ce = law.CSVParameter(default=["RWTH"], significant=False, description="target computing "
        "element(s)")

    exclude_params_branch = {"grid_ce"}

    @classmethod
    def modify_param_values(cls, params):
        params = AnalysisTask.modify_param_values(params)
        if "workflow" in params and law.is_no_param(params["workflow"]):
            ces = [cls.glite_ce_map.get(ce, ce) for ce in params["grid_ce"]]
            params["glite_ce"] = tuple(law.util.flatten(ces))
            params["workflow"] = "glite"
        return params

    def glite_workflow_requires(self):
        reqs = super(GridWorkflow, self).glite_workflow_requires()
        reqs["cmssw"] = UploadCMSSW.req(self, replicas=10, _prefer_cli=["replicas"])
        reqs["software"] = UploadSoftware.req(self, replicas=10, _prefer_cli=["replicas"])
        reqs["repo"] = UploadRepo.req(self, replicas=10, _prefer_cli=["replicas"])
        return reqs

    def glite_output_directory(self):
        return law.WLCGDirectoryTarget(self.wlcg_path())

    def glite_output_uri(self):
        return self.glite_output_directory().url(cmd="listdir")

    def glite_bootstrap_file(self):
        return law.util.rel_path("$SDHA_BASE", "grid_bootstrap.sh")

    def glite_job_config(self, config, job_num, branches):
        config = super(GridWorkflow, self).glite_job_config(config, job_num, branches)

        # load the workflow requirements as we need to pass the remote software paths
        # as render variables to all job files
        reqs = self.glite_workflow_requires()

        # render variables
        config.render_variables["sdha_grid_user"] = os.getenv("SDHA_GRID_USER")
        config.render_variables["scram_arch"] = os.getenv("SCRAM_ARCH")
        config.render_variables["cmssw_base_url"] = reqs["cmssw"].output().dir.url()
        config.render_variables["cmssw_version"] = os.getenv("CMSSW_VERSION")
        config.render_variables["software_base_url"] = reqs["software"].output().dir.url()
        config.render_variables["repo_checksum"] = reqs["repo"].checksum
        config.render_variables["repo_base"] = reqs["repo"].output().dir.url()

        # the vo
        config.vo = os.getenv("SDHA_GRID_VO", "cms:/cms/dcms")

        return config


class UploadCMSSW(AnalysisTask, law.BundleCMSSW, law.TransferLocalFile):

    force_upload = luigi.BoolParameter(default=False, description="force uploading")

    # settings for BunddleCMSSW
    cmssw_path = os.environ["CMSSW_BASE"]

    # settings for TransferLocalFile
    source_path = None

    version = None

    def __init__(self, *args, **kwargs):
        super(UploadCMSSW, self).__init__(*args, **kwargs)

        self.has_run = False

    def complete(self):
        if self.force_upload and not self.has_run:
            return False
        else:
            return super(UploadCMSSW, self).complete()

    def single_output(self):
        path = "{}.tgz".format(os.path.basename(self.cmssw_path))
        return self.wlcg_target(path)

    def output(self):
        return law.TransferLocalFile.output(self)

    def run(self):
        bundle = law.LocalFileTarget(is_tmp="tgz")
        self.bundle(bundle)
        self.transfer(bundle)

        self.has_run = True


class UploadSoftware(AnalysisTask, law.TransferLocalFile):

    version = None

    source_path = os.environ["SDHA_SOFTWARE"] + ".tgz"

    def single_output(self):
        return self.wlcg_target("software.tgz")

    def run(self):
        # create the local bundle
        bundle = law.LocalFileTarget(self.source_path, is_tmp=True)
        def _filter(tarinfo):
            return None if re.search(r"(\.pyc|\/\.git|\.tgz)$", tarinfo.name) else tarinfo
        bundle.dump(os.path.splitext(self.source_path)[0], filter=_filter)
        self.publish_message("bundled software archive")

        # super run will upload all files for us
        super(UploadSoftware, self).run()


class UploadRepo(AnalysisTask, law.BundleGitRepository, law.TransferLocalFile):

    # settings for BundleGitRepository
    repo_path = os.environ["SDHA_BASE"]

    # settings for TransferLocalFile
    source_path = None

    version = None

    def single_output(self):
        path = "{}.{}.tgz".format(os.path.basename(self.repo_path), self.checksum)
        return self.wlcg_target(path)

    def output(self):
        return law.TransferLocalFile.output(self)

    def run(self):
        bundle = law.LocalFileTarget(is_tmp="tgz")
        self.bundle(bundle)
        self.transfer(bundle)


class HTCondorJobManagerRWTH(law.HTCondorJobManager):

    @classmethod
    def map_status(cls, status_flag):
        # map status hold ("5", "H") and suspended ("7") to
        # cls.PENDING to avoid resubmission
        if status_flag in ("0", "1", "5", "7", "U", "I", "H"):
            return cls.PENDING
        elif status_flag in ("2", "R"):
            return cls.RUNNING
        elif status_flag in ("4", "C"):
            return cls.FINISHED
        elif status_flag in ("6", "E"):
            return cls.FAILED
        else:
            return cls.FAILED


class HTCondorWorkflow(law.HTCondorWorkflow):
    """
    Batch systems are typically very heterogeneous by design, and so is HTCondor. Law does not aim
    to "magically" adapt to all possible HTCondor setups which would certainly end in a mess.
    Therefore we have to configure the base HTCondor workflow in law.contrib.htcondor to work with
    the RWTH HTCondor environment. In most cases, like in this example, only a minimal amount of
    configuration is required.
    """
    htcondor_logs = luigi.BoolParameter()

    def htcondor_post_submit_delay(self):
        return self.poll_interval * 60

    def htcondor_output_directory(self):
        # the directory where submission meta data should be stored
        return law.LocalDirectoryTarget(self.local_path())

    def htcondor_bootstrap_file(self):
        # each job can define a bootstrap file that is executed prior to the actual job
        # in order to setup software and environment variables
        return law.util.rel_path("$SDHA_BASE", "htcondor_bootstrap.sh")

    def htcondor_job_config(self, config, job_num, branches):
        # render_data is rendered into all files sent with a job
        config.render_variables["sdha_base"] = os.getenv("SDHA_BASE")
        # copy the entire environment
        config.custom_content.append(("getenv", "true"))
        # condor logs
        if self.htcondor_logs:
            config.stdout = "out.txt"
            config.stderr = "err.txt"
            config.log = "log.txt"
        return config

    def htcondor_use_local_scheduler(self):
        return True

    def htcondor_create_job_manager(self, **kwargs):
        kwargs = law.util.merge_dicts(self.htcondor_job_manager_defaults, kwargs)
        return HTCondorJobManagerRWTH(**kwargs)
